# Scope Builds
This project has the packaged versions of the Scope IDE.  Scope is a portable app with no installer. For the source goto the main [Scope](https://gitlab.com/lucidlylogicole/scope) project.

## Instructions
1. Download the zip for your system (below)
2. Extract to a directory of your choice
3. Set the `Scope.sh` permission to executable (Linux only)
4. Run the `Scope.sh` (Linux) or `Scope.exe` (Windows)

## Downloads

### [Linux (x64)](https://gitlab.com/lucidlylogicole/scope-builds/-/blob/master/dist/scope-linux-x64.zip)
### [Raspberry Pi (arm7l)](https://gitlab.com/lucidlylogicole/scope-builds/-/blob/master/dist/scope-linux-armv7l.zip)
### [Windows (x64)](https://gitlab.com/lucidlylogicole/scope-builds/-/blob/master/dist/scope-win32-x64.zip)

## Update Scope
- To update scope, download the latest build or run the `Scope/update_scope.sh` script

## License
[License](/media/blackbeard/bb_apps/scope/LICENSE)  - GNU General Public License (GPL 3)